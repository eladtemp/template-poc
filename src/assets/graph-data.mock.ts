import GraphData, { GraphTypes } from "src/model/graphdata.interface";

const GraphDataMock : GraphData = {
    name : 'Sales',
    nav : {
        month : {
            otherinfo : '',
            slides : [{
                title : 'slide 1',
                data : {
                    type : GraphTypes.BASETABLE,
                    title : 'graphdata 1',
                    name : 'name here 1',
                    description : 'description here 1'
                }
            },{
                title : 'slide 2',
                data : {
                    type : GraphTypes.MULTIPLEROWS,
                    title : 'graphdata 2',
                    name : 'name here 2',
                    description : 'description here 2'
                }
            }
        ]
        },
        qtd : {
          otherinfo : '',
          slides : []
        },
        ytd : {
          otherinfo : '',
          slides : []
        }
    }
}

export default GraphDataMock;
