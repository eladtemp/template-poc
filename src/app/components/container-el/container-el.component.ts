import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import GraphData from 'src/model/graphdata.interface';

@Component({
  selector: 'app-container-el',
  templateUrl: './container-el.component.html',
  styleUrls: ['./container-el.component.scss']
})
export class ContainerElComponent implements OnInit {

  @Input()
  itemTemplate!: TemplateRef<any>;

  @Input()
  containerData !: GraphData;

  currentSlide = 0;
  setSlide = (indexToSet : number) => {
    this.currentSlide = indexToSet;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
