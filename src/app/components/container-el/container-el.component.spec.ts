import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerElComponent } from './container-el.component';

describe('ContainerElComponent', () => {
  let component: ContainerElComponent;
  let fixture: ComponentFixture<ContainerElComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainerElComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerElComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
