import { Component, Input, OnInit } from '@angular/core';
import GraphData, { GraphTypes } from 'src/model/graphdata.interface';

@Component({
  selector: 'app-inner-component',
  templateUrl: './inner-component.component.html',
  styleUrls: ['./inner-component.component.scss']
})
export class InnerComponentComponent implements OnInit {

  @Input() data !: GraphData;
  graphTypes = GraphTypes;
  JSON = JSON;


  constructor() {

  }

  ngOnInit(): void {
  }

}
