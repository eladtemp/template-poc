import { Component } from '@angular/core';
import GraphDataMock from 'src/assets/graph-data.mock';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  mockdata = GraphDataMock;
  title = 'ngtemp-proj';
}
