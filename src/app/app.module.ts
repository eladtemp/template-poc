import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContainerElComponent } from './components/container-el/container-el.component';
import { InnerComponentComponent } from './components/inner-component/inner-component.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerElComponent,
    InnerComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
